﻿using System.Collections.Generic;
using System;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria;

namespace MeleeToThrow {
	public class MeleeToThrow : Mod {
		public MeleeToThrow() {
			Properties = new ModProperties {
				Autoload = true
			};
		}
	}

	public static class Config {
		public static string[] ToThrow = {
			ItemID.MagicDagger.ToString(),
		};

		public static string[] ToRanged = {
			ItemID.GolemFist.ToString(),
			ItemID.KOCannon.ToString(),
			"SpiritMod.RealityDefier",
		};
		
		public static string[] ToNone = {
			"GRealm.DiamondDrill",
			"GRealm.ThornDrill",
		};
		
		public static string[] IgnoreMe = {
			"CalamityMod.BansheeHook",
			"CalamityMod.CosmicDischarge",
			"CalamityMod.Murasama",
			"CalamityMod.Nebulash",
			"CalamityMod.TyphoonsGreed",
			"Disarray.MagmiteEncircler",
			"Disarray.TectoniteEncircler",
			"ElementsAwoken.TheValkyrie",
			"GRealm.GlowSpear",
			"GRealm.NagaPolleni",
			"GRealm.SpearOfDecay",
			"GRealm.TikiPike",
			"Pumpking.Spearon",
			"Pumpking.TerraDragoon",
			"SacredTools.DragonDrill",
			"SacredTools.Perjordian",
			"SacredTools.Phaselash",
			"SacredTools.PlasmaCleaver",
			"SacredTools.SerpentChain",
			"SacredTools.ShadowflareMidnight",
		};
	}

	public static class Functions {
		public static bool IsThrowable(Item item) {
			bool result = (item.melee && item.shoot > 0 && item.noUseGraphic && item.useStyle != 100 && item.useStyle != 300 && item.useStyle != 500);
			Projectile projectile = new Projectile();
			projectile.SetDefaults(item.shoot);
			if (projectile.aiStyle == 19 || projectile.aiStyle == 75 || projectile.aiStyle == 140 || projectile.aiStyle == 141 || projectile.aiStyle == 142 || projectile.aiStyle == 143) { result = false; }
			return result;
		}
		
		public static bool IsTool(Item item) {
			return (item.pick > 0 || item.hammer > 0 || item.axe > 0 || item.fishingPole > 0);
		}
	}
	
	public class MeleeToThrowSetup : GlobalItem {
		public override bool CloneNewInstances { get { return true; } }
		public override bool InstancePerEntity { get { return true; } }
		public bool fromMelee = false;
		public bool fromRanged = false;
		public bool fromMagic = false;
		public bool wasThrowable = false;
		public override void SetDefaults(Item item) {
			fromMelee = item.melee;
			fromRanged = item.ranged;
			fromMagic = item.magic;
			if (Array.IndexOf(Config.IgnoreMe,DALib.Functions.GetItemID(item)) == -1) {
				if (Functions.IsTool(item) || Array.IndexOf(Config.ToNone,DALib.Functions.GetItemID(item)) != -1) {
					item.magic = false;
					item.melee = false;
					item.ranged = false;
					item.summon = false;
					item.thrown = false;
				}
				else if (item.damage > 0) {
					if (Functions.IsThrowable(item)) {
						item.damage -= item.mana;
						wasThrowable = true;
						item.magic = false;
						item.melee = false;
						item.ranged = false;
						item.summon = false;
						item.thrown = true;
						item.mana = 0;
					}
					else if (Array.IndexOf(Config.ToThrow,DALib.Functions.GetItemID(item)) != -1) {
						item.damage -= item.mana;
						item.magic = false;
						item.melee = false;
						item.ranged = false;
						item.summon = false;
						item.thrown = true;
						item.mana = 0;
					}
					else if (Array.IndexOf(Config.ToRanged,DALib.Functions.GetItemID(item)) != -1) {
						item.damage -= item.mana;
						item.magic = false;
						item.melee = false;
						item.ranged = true;
						item.summon = false;
						item.thrown = false;
						item.mana = 0;
					}
				}
			}
		}
		
		public override void ModifyTooltips(Item item, List<TooltipLine> tooltips) {
			if (item.type == 0) { return; }
			if (!DALib.Config.Global.Debug) { return; }
			if (Array.IndexOf(Config.IgnoreMe,DALib.Functions.GetItemID(item)) == -1) {
				if (Array.IndexOf(Config.ToThrow,DALib.Functions.GetItemID(item)) != -1 || Array.IndexOf(Config.ToRanged,DALib.Functions.GetItemID(item)) != -1) {
					var line = new TooltipLine(this.mod, "M2T", "Damage Type converted from " + (fromMelee ? "melee" : fromRanged ? "ranged" : fromMagic ? "magic" : "unknown") + " to " + (item.melee ? "melee" : item.ranged ? "ranged" : item.magic ? "magic" : item.summon ? "summon" : item.thrown ? "thrown" : "unknown") + " by Static List");
					line.overrideColor = Colors.RarityBlue;
					tooltips.Add(line);
				}
				if (wasThrowable) {
					var line = new TooltipLine(this.mod, "M2Tx", "Damage Type converted from " + (fromMelee ? "melee" : fromRanged ? "ranged" : fromMagic ? "magic" : "unknown") + " to " + (item.melee ? "melee" : item.ranged ? "ranged" : item.magic ? "magic" : item.summon ? "summon" : item.thrown ? "thrown" : "unknown") + " based on Item Data");
					line.overrideColor = Colors.RarityOrange;
					tooltips.Add(line);
				}
			}
			else {
				var line = new TooltipLine(this.mod, "M2T", "Item is excluded from damage type conversion");
				line.overrideColor = Colors.RarityRed;
				tooltips.Add(line);
			}
		}
	}
}
